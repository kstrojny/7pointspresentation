package com.sevenPoints.red5Server.security;

import com.mongodb.*;
import com.sevenPoints.red5Server.MongoProxy;
import com.sevenPoints.red5Server.status.Status;
import com.sevenPoints.red5Server.status.StatusManager;
import org.red5.logging.Red5LoggerFactory;
import org.slf4j.Logger;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by szymon on 09.10.14.
 */
public class StreamAuth {
    private String userId = null;
    private String videoId= null;
    private String token = null;

    //get envs for db connection
    private static final String backdoorToken = "superBigFish2000";
    private static String dbUrl = MongoProxy.getDbUrl();
    private static String dbPort = MongoProxy.getDbPort();
    private static String dbUserName = MongoProxy.getDbUserName();
    private static String dbPassword = MongoProxy.getDbPassword();
    private static String dbName = MongoProxy.getDbName();

    private static Logger logger = Red5LoggerFactory.getLogger(StreamPublishingSecurity.class, "7points-red5server");


    public StreamAuth(String name) {
        Pattern pattern = Pattern.compile("(\\S+):(\\S+):(\\S+)");

        //Match pattern to stream name
        Matcher matcher = pattern.matcher(name);

        if (matcher.find()) {
            //find groups if pattern matched
            videoId = matcher.group(1);
            userId = matcher.group(2);
            token = matcher.group(3);
        }
    }

    public boolean check(){
        ServerAddress address = null;
        MongoClient mongoClient = null;

        logger.info("video ID:" + videoId);

        if (this.userId != null && this.videoId != null && this.token != null) {

            //try to connect to database
            try {
                address = new ServerAddress(dbUrl, Integer.parseInt(dbPort));
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (address != null) {
                //If DB server connected try to sign in
                MongoCredential credential = MongoCredential.createMongoCRCredential(dbUserName, dbName, dbPassword.toCharArray());
                mongoClient = new MongoClient(Arrays.asList(address), Arrays.asList(credential));
                mongoClient.setReadPreference(ReadPreference.primaryPreferred());
                if(mongoClient != null) {
                    //If mongo client is connected and signed in to server try to get user token from DB.
                    DB db = mongoClient.getDB(dbName);
                    DBCollection users = db.getCollection("users");
                    DBCollection videos = db.getCollection("videos");

                    //Find user by ID
                    BasicDBObject query = new BasicDBObject("_id", userId);
                    DBObject user = null;
                    user = users.findOne(query);

                    //Find video by ID
                    BasicDBObject fileQuery = new BasicDBObject("_id", videoId);
                    DBObject video = null;
                    video = videos.findOne(fileQuery);

                    if (video != null) {
                        String video_userId = (video.get("userId")).toString();
                        if(!userId.equals(video_userId)){
                            logger.error("video have different user in database {} {}", userId, video_userId);
                                StatusManager.setStatus(videoId, Status.UNAUTHORIZED);
                            return false;
                        } else {
                            logger.debug("video validation OK");
                        }
                    } else {
                        logger.error("video {} do not exist in database", videoId);
                        return false;
                    }

                    if(user != null) {
                        //If user exists get the token and compare it with token from stream

                        if(token.equals(this.backdoorToken)){
                            logger.info("File: " + videoId + " authorized.");
                            return true;
                        }
                        String dbToken = null;
                        try {
                            dbToken = ((DBObject) (((DBObject) user.get("services")).get("video"))).get("token").toString();
                        }catch (Exception e){
                            logger.error("File: " + videoId + " unauthorized.");
                            StatusManager.setStatus(videoId, Status.UNAUTHORIZED);
                            return false;
                        }
                        if(dbToken != null) {
                            BasicDBObject dBObject = new BasicDBObject();
                            BasicDBObject unset = new BasicDBObject("$unset", dBObject);
                            dBObject.put("services.video.token", true);
                            users.update(query, unset);
                            if (dbToken.equals(token)) {
                                //Token valid get the record
                                logger.info("File: " + videoId + " authorized.");
                                return true;
                            }
                        }
                        //Token invalid stop! Don't record stream.
                        logger.info("File: " + videoId + " unauthorized.");
                        StatusManager.setStatus(videoId, Status.UNAUTHORIZED);
                        return false;
                    }else{
                        //User don't exists
                        logger.error("User with id {} not exists!", userId);
                    }
                } else {
                    logger.error("mongoClient is null");
                }
            } else {
                logger.error("ServerAddress is null");
            }
        }

        return false;
    }

    public String getUsrId() {
        return userId;
    }

    public void setUsrId(String usrId) {
        this.userId = usrId;
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
