package com.sevenPoints.red5Server.security;

import org.red5.server.api.IScope;
import org.red5.server.api.stream.IStreamFilenameGenerator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by szymon on 25.09.14.
 */
public class FilenameGenerator  implements IStreamFilenameGenerator {
    /** Path that will store recorded videos. */
    public String path = "streams/";

    /** Set if the path is absolute or relative */
    public boolean resolvesAbsolutePath = false;

    @Override
    public String generateFilename(IScope scope, String name, GenerationType type) {
        // Generate filename without an extension.
        return generateFilename(scope, name, null, type);
    }

    @Override
    public String generateFilename(IScope scope, String name, String extension, GenerationType type) {

        Pattern pattern = Pattern.compile("(\\S+):(\\S+):\\S+");
        Matcher matcher = pattern.matcher(name);
        String fileName = null;

        if (matcher.find()) {
            fileName = matcher.group(2) + "_" + matcher.group(1);
        }else{
            fileName = name;
        }

        String filename = path + fileName;

        if (extension != null)
            // Add extension
            filename += extension;

        return filename;
    }

    @Override
    public boolean resolvesToAbsolutePath()
    {
        return resolvesAbsolutePath;
    }
}
