package com.sevenPoints.red5Server.security;



import com.sevenPoints.red5Server.security.StreamAuth;
import org.red5.server.api.IScope;
import org.red5.server.api.stream.IStreamPublishSecurity;


/**
 * Created by szymon on 24.09.14.
 */
public class StreamPublishingSecurity implements IStreamPublishSecurity  {


    public boolean isPublishAllowed(IScope scope, String name, String mode) {
        StreamAuth authSystem = new StreamAuth(name);
        return authSystem.check();
    }


}
