package com.sevenPoints.red5Server;

import com.mongodb.*;
import com.sevenPoints.red5Server.status.Status;
import org.apache.commons.io.FilenameUtils;
import org.red5.logging.Red5LoggerFactory;
import org.slf4j.Logger;

import java.nio.file.FileSystems;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by vreal on 29.09.14.
 */
public class MongoProxy {
    private static Logger logger = Red5LoggerFactory.getLogger(MongoProxy.class, "7points-red5server");

    private static String dbUrl;
    private static String dbPort;
    private static String dbUserName;
    private static String dbPassword;
    private static String dbName;

    static{
        String MONGO_REMOTE_URL = System.getenv("MONGO_REMOTE_URL");
        Pattern pattern = Pattern.compile("mongodb://(\\S+):(\\S+)@(\\S+):(\\S+)/(\\S+)");
        Matcher matcher = pattern.matcher(MONGO_REMOTE_URL);
        if (matcher.find()) {
            dbUserName = matcher.group(1);
            dbPassword = matcher.group(2);
            dbUrl = matcher.group(3);
            dbPort = matcher.group(4);
            dbName = matcher.group(5);
        } else {
            logger.error("Mongo remote url does not match pattern");
            System.exit(1);
        }
    }

    public static void saveUrlsInDatabase(String path, Map<String, String> urlsMap){
        ServerAddress address = null;
        MongoClient mongoClient = null;

        //try to connect to database
        try {
            address = new ServerAddress(dbUrl, Integer.parseInt(dbPort));
        } catch (Exception e) {
            logger.error("Failed to create Server Address", e);
        }

        if (address != null) {
            //If DB server connected try to sign in
            MongoCredential credential = MongoCredential.createMongoCRCredential(dbUserName, dbName, dbPassword.toCharArray());
            mongoClient = new MongoClient(Arrays.asList(address), Arrays.asList(credential));
            mongoClient.setReadPreference(ReadPreference.primaryPreferred());
            if(mongoClient != null) {

                //If mongo client is connected and signed in to server try to get user token from DB.
                DB db = mongoClient.getDB(dbName);
                DBCollection coll = db.getCollection("videos");

                String objectKey = FilenameUtils.getBaseName(path);

                String videoId = objectKey.substring(objectKey.indexOf('_') + 1);  //example :TzZSy6v3TuPZsg4vR_mXTEcotKDazzJjoJ8.

                //Find user by ID
                BasicDBObject query = new BasicDBObject("_id", videoId);
                DBObject video = null;
                video = coll.findOne(query);


                if(video != null) {
                    BasicDBObject dBObject = new BasicDBObject();
                    BasicDBObject dBObject2 = new BasicDBObject();
                    BasicDBObject set = new BasicDBObject("$set", dBObject);
                    for(String key : urlsMap.keySet()){
                        dBObject.put("url." + key, urlsMap.get(key));
                    }
                    dBObject2.put("number", Status.FINISH.getStatusNumber());
                    dBObject2.put("message", Status.FINISH.getMessage());
                    dBObject.put("status",dBObject2);
                    SimpleDateFormat df = new SimpleDateFormat(Application.getProp().getProperty("date_format"));
                    dBObject.put("uploadedAt", df.format(new Date()));
                    coll.update(query, set);
                } else {
                    //User don't exists
                   logger.error("Video {} not exists!", videoId);
                }
            } else {
                logger.error("mongoClient is null");
            }
        } else {
            logger.error("ServerAddress is null");
        }
    }

    public static String getDbName() {
        return dbName;
    }

    public static String getDbUrl() {
        return dbUrl;
    }

    public static String getDbPort() {
        return dbPort;
    }

    public static String getDbUserName() {
        return dbUserName;
    }

    public static String getDbPassword() {
        return dbPassword;
    }


}
