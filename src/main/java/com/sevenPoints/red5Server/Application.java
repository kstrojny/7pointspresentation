package com.sevenPoints.red5Server;

import com.sevenPoints.red5Server.security.StreamPublishingSecurity;
import org.red5.logging.Red5LoggerFactory;
import org.red5.server.adapter.MultiThreadedApplicationAdapter;
import org.red5.server.api.IConnection;
import org.red5.server.api.IScope;
import org.red5.server.api.stream.IBroadcastStream;
import org.slf4j.Logger;

import java.io.*;
import java.util.Properties;

public class Application extends MultiThreadedApplicationAdapter {

    public static final String appPath = "webapps/7points-red5server/";
    private static final String propertiesPath = appPath + "WEB-INF/App.properties";
    private static Logger logger = Red5LoggerFactory.getLogger(Application.class, "7points-red5server");
    private static Properties prop;

    @Override
    public boolean appStart(IScope app) {;
        logger.info("App Started");
        prop = new Properties();
        try {
            prop.load(new FileInputStream(propertiesPath));
        } catch (FileNotFoundException e) {
            logger.error("Could not locate properties file in " + new File(propertiesPath).getAbsoluteFile());
        } catch (IOException e) {
            logger.error("IOException while reading file in " + new File(propertiesPath).getAbsoluteFile());
        }
        registerStreamPublishSecurity(new StreamPublishingSecurity());
        return true;
    }

    @Override
    public boolean appConnect(IConnection conn, Object[] params) {
        logger.info("appConnect {}", conn);
        return super.appConnect(conn, params);

    }

    @Override
    public void streamBroadcastClose(IBroadcastStream stream){
        logger.info("StreamBroadcastClose " + stream.getSaveFilename());
        String path = appPath + stream.getSaveFilename();

        Starter starter = new Starter(path);
        new Thread(starter).start();

    }

    @Override
    public void appDisconnect(IConnection conn) {
        logger.info("App Disconnect " + conn);
        super.appDisconnect(conn);
    }

    public static Properties getProp() {
        return prop;
    }
}