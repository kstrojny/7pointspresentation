package com.sevenPoints.red5Server;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.amazonaws.util.StringUtils;
import com.sevenPoints.red5Server.security.StreamAuth;
import com.sevenPoints.red5Server.status.Status;
import com.sevenPoints.red5Server.status.StatusManager;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.ProgressListener;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.red5.logging.Red5LoggerFactory;
import org.slf4j.Logger;

import java.io.*;
import java.util.*;

/**
 * Created by vreal on 06.10.14.
 */

public class FileUploadServlet extends HttpServlet {
    private static Logger logger = Red5LoggerFactory.getLogger(FileUploadServlet.class, "7points-red5server");
    private static String uploadFolder = "uploads/";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doGet(request, response);
        logger.info("doGet method invoked");
        doPost(request,response);
    }

    @Override
    public void doPost( HttpServletRequest request, HttpServletResponse response )  {
        String videoId = null;
        Properties prop = Application.getProp();
        logger.info("doPost method invoked");
        Map<String, String> attributes = new HashMap<>();
        String uploadedFileName = "";
        String uploadedPath = "";

        /**
         * Create a factory for new disk-based file items
         */
        FileItemFactory factory = new DiskFileItemFactory();
        /**
         * Create a new file upload handler
         */
        ServletFileUpload upload = new ServletFileUpload(factory);
        upload.setProgressListener(new ProgressListener(){
            private long megaBytes = -1;
            public void update(long pBytesRead, long pContentLength, int pItems) {
                long mBytes = pBytesRead / 2000000;
                if (megaBytes == mBytes) {
                    return;
                }
                megaBytes = mBytes;
                logger.debug("We are currently reading item " + pItems);
                if (pContentLength == -1) {
                    logger.debug("So far, " + pBytesRead + " bytes have been read.");
                } else {
                    logger.debug("So far, " + pBytesRead + " of " + pContentLength
                            + " bytes have been read.");
                }
            }
        });

        try {
            /**
             * Parsing input request
             */
            List items = upload.parseRequest(request);
            /**
             * Process the uploaded items
             */
            Iterator formFieldIter = items.iterator();
            Iterator notFormFieldIter = items.iterator();
            while (formFieldIter.hasNext()) {
                FileItem item = (FileItem) formFieldIter.next();
                if (item.isFormField()){
                    attributes.put(item.getFieldName(), item.getString());
                }
            }

            videoId = attributes.get("videoId");
            String userId = attributes.get("userId");
            String token = attributes.get("token");

            StreamAuth authSystem = new StreamAuth(videoId + ":" + userId + ":" + token);
            if(authSystem.check()) {
                while (notFormFieldIter.hasNext()) {
                    FileItem item = (FileItem) notFormFieldIter.next();
                    if (!item.isFormField()) {

                        /*
                        * Check if file is not too big
                        */
                        long maxSize = Long.parseLong(prop.getProperty("upload_max_size"));
                        if (maxSize < item.getSize()) {
                            logger.error("File {} is too big, {} is more than max {}", item.getName(), item.getSize(), maxSize);
                        }
                        /**
                         * handling file uploads
                         */
                        uploadedFileName = item.getName();

                        uploadedPath = Application.appPath + uploadFolder + userId + "_" + videoId + "." + FilenameUtils.getExtension(item.getName());
                        File uploadedFile = new File(uploadedPath);
                        try {
                            item.write(uploadedFile);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                if (StringUtils.isNullOrEmpty(uploadedFileName)) {
                    logger.error("There was no uploaded file");
                } else {
                    new Starter(uploadedPath).run();
                }
            }
            else {
                response.getWriter().write("Error");
                response.getWriter().close();
            }
        }
        catch(Exception exception) {
            exception.printStackTrace();
            if(videoId != null )
                StatusManager.setStatus(videoId, Status.INTERNAL_SERVER_ERROR);
        }
    }
}