package com.sevenPoints.red5Server.status;

/**
 * Created by vreal on 13.10.14.
 */
public enum Status {
    CREATE(201, "Create"),
    UPLOAD(230, "Upload"),
    FINISH(231, "Finish"),
    UNAUTHORIZED(401, "Unauthorized"),
    INTERNAL_SERVER_ERROR(500, "Internal server error");

    private int statusNumber;
    private String message;

    Status(int statusNumber, String message){
        this.statusNumber = statusNumber;
        this.message = message;
    }

    public int getStatusNumber() {
        return statusNumber;
    }

    public String getMessage() {
        return message;
    }
}
