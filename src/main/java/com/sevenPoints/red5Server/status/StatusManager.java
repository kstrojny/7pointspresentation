package com.sevenPoints.red5Server.status;

import com.mongodb.*;
import com.sevenPoints.red5Server.Application;
import com.sevenPoints.red5Server.MongoProxy;
import org.red5.logging.Red5LoggerFactory;
import org.slf4j.Logger;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

/**
 * Created by szymon on 10.10.14.
 */
public class StatusManager {

    private static Logger logger = Red5LoggerFactory.getLogger(StatusManager.class, "7points-red5server");

    public static void setStatus(String videoId, Status status){
        logger.info("Setting {} to databasse", status.getMessage());
        String dbUrl = MongoProxy.getDbUrl();
        String dbPort = MongoProxy.getDbPort();
        String dbUserName = MongoProxy.getDbUserName();
        String dbPassword = MongoProxy.getDbPassword();
        String dbName = MongoProxy.getDbName();

        ServerAddress address = null;
        MongoClient mongoClient = null;

        try {
            address = new ServerAddress(dbUrl, Integer.parseInt(dbPort));
        } catch (Exception e) {
            logger.error("Failed to create Server Address", e);
        }

        if (address != null) {
            //If DB server connected try to sign in
            MongoCredential credential = MongoCredential.createMongoCRCredential(dbUserName, dbName, dbPassword.toCharArray());
            mongoClient = new MongoClient(Arrays.asList(address), Arrays.asList(credential));
            mongoClient.setReadPreference(ReadPreference.primaryPreferred());
            if(mongoClient != null) {
                //If mongo client is connected and signed in to server try to get user token from DB.
                DB db = mongoClient.getDB(dbName);
                DBCollection coll = db.getCollection("videos");

                //Find user by ID
                BasicDBObject query = new BasicDBObject("_id", videoId);
                DBObject video = null;
                video = coll.findOne(query);


                if(video != null) {
                    BasicDBObject dBObject = new BasicDBObject();
                    BasicDBObject dBObject2 = new BasicDBObject();
                    BasicDBObject set = new BasicDBObject("$set", dBObject);
                    dBObject.put("status",dBObject2);
                    dBObject2.put("number", status.getStatusNumber());
                    dBObject2.put("message", status.getMessage());
                    SimpleDateFormat df = new SimpleDateFormat(Application.getProp().getProperty("date_format"));
                    dBObject.put("uploadedAt", df.format(new Date()));
                    coll.update(query, set);
                    logger.info("Video {} status updated.", videoId);
                } else {
                    //User don't exists
                    logger.error("Video {} not exists!", videoId);
                }
            } else {
                logger.error("mongoClient is null");
            }
        } else {
            logger.error("ServerAddress is null");
        }
    }
}
