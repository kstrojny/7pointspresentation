package com.sevenPoints.red5Server.converter;

import com.xuggle.mediatool.IMediaReader;
import com.xuggle.mediatool.IMediaWriter;
import com.xuggle.mediatool.MediaToolAdapter;
import com.xuggle.mediatool.event.*;
import com.xuggle.xuggler.*;

/**
 * Created by vreal on 10.10.14.
 * source: http://whaticode.com/2010/04/30/use-java-to-convert-any-media-type-to-flv-with-xuggler-part-2/
 */
public class FlashMediaAdapter extends MediaToolAdapter {

    private IVideoResampler videoResampler = null;
    private IAudioResampler audioResampler = null;
    private IMediaReader mediaReader;
    private IMediaWriter mediaWriter;

    public FlashMediaAdapter(IMediaReader mediaReader, IMediaWriter mediaWriter){
        this.mediaReader = mediaReader;
        this.mediaWriter = mediaWriter;
    }

    @Override
    public void onAddStream(IAddStreamEvent event) {
        int streamIndex = event.getStreamIndex();
        IStreamCoder streamCoder = event.getSource().getContainer().getStream(streamIndex).getStreamCoder();
        if (streamCoder.getCodecType() == ICodec.Type.CODEC_TYPE_AUDIO) {
            mediaWriter.addAudioStream(streamIndex, streamIndex, 2, 44100);
        } else if (streamCoder.getCodecType() == ICodec.Type.CODEC_TYPE_VIDEO) {
            streamCoder.setWidth(streamCoder.getWidth());
            streamCoder.setHeight(streamCoder.getHeight());
            mediaWriter.addVideoStream(streamIndex, streamIndex, streamCoder.getWidth(), streamCoder.getHeight());
        }
        super.onAddStream(event);
    }

    @Override
    public void onVideoPicture(IVideoPictureEvent event) {
        IVideoPicture pic = event.getPicture();
        if (videoResampler == null) {
            videoResampler = IVideoResampler.make(pic.getWidth(), pic.getHeight(), pic.getPixelType(), pic.getWidth(), pic.getHeight(), pic.getPixelType());
        }
        IVideoPicture out = IVideoPicture.make(pic.getPixelType(), pic.getWidth(), pic.getHeight());
        videoResampler.resample(out, pic);

        IVideoPictureEvent asc = new VideoPictureEvent(event.getSource(), out, event.getStreamIndex());
        super.onVideoPicture(asc);
        out.delete();
    }

    @Override
    public void onAudioSamples(IAudioSamplesEvent event) {
        IAudioSamples samples = event.getAudioSamples();
        if (audioResampler == null) {
            //44100 is magic number to allow conversion to flv format
            audioResampler = IAudioResampler.make(2, samples.getChannels(), 44100, samples.getSampleRate());
        }
        if (event.getAudioSamples().getNumSamples() > 0) {
            IAudioSamples out = IAudioSamples.make(samples.getNumSamples(), samples.getChannels());
            audioResampler.resample(out, samples, samples.getNumSamples());
            AudioSamplesEvent asc = new AudioSamplesEvent(event.getSource(), out, event.getStreamIndex());
            super.onAudioSamples(asc);
            out.delete();
        }
    }
}
