package com.sevenPoints.red5Server.converter;

import com.sevenPoints.red5Server.Application;
import com.sevenPoints.red5Server.status.Status;
import com.sevenPoints.red5Server.status.StatusManager;
import com.xuggle.mediatool.MediaListenerAdapter;
import com.xuggle.mediatool.event.IVideoPictureEvent;
import com.xuggle.xuggler.Global;
import org.apache.commons.io.FilenameUtils;
import org.red5.logging.Red5LoggerFactory;
import org.slf4j.Logger;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by vreal on 10.10.14.
 * This class create thumbnail from frame at MICRO_SECONDS_CAPTURE_FRAME and stores it in thumbnail directory
 */
public class ImageSnapListener extends MediaListenerAdapter {
    private static Logger logger = Red5LoggerFactory.getLogger(ImageSnapListener.class, "7points-red5server");
    private static final long MICRO_SECONDS_CAPTURE_FRAME = 1000000 * 1;        //frame taken after 1s
    private static final String thumbnailPath = "thumbnails/";
    public static final String THUMBNAIL_FORMAT = "jpg";

    private String thumbnailFilePath = "";
    private String filePath;
    private boolean alreadyCreated = false;
    private int mVideoStreamIndex = -1;
    private boolean capture = false;

    public ImageSnapListener(String filePath){
        this.filePath = filePath;
    }

    public void onVideoPicture(IVideoPictureEvent event) {;
        if (event.getStreamIndex() != mVideoStreamIndex) {
            // if the selected video stream id is not yet set, go ahead an
            // select this lucky video stream
            if (mVideoStreamIndex == -1)
                mVideoStreamIndex = event.getStreamIndex();
                // no need to show frames from this video stream
            else
                return;
        }
        // if it's time to write the frame
        if (!capture && event.getTimeStamp()  >= MICRO_SECONDS_CAPTURE_FRAME
                && !alreadyCreated) {//thumbnail was not already created
            String outputFilename = dumpImageToFile(event.getImage());
            // indicate file written
            double seconds = ((double) event.getTimeStamp()) /
                    Global.DEFAULT_PTS_PER_SECOND;
            logger.info("At elapsed time of {} seconds wrote: {}\n",
                    seconds, outputFilename);
            capture = true;
        }
    }

    private String dumpImageToFile(BufferedImage image) {
        String videoId = null;
        try {
            String fileName = FilenameUtils.getBaseName(filePath);
            fileName = FilenameUtils.removeExtension(fileName) + "." + THUMBNAIL_FORMAT;
            String outputFilePath = Application.appPath + thumbnailPath + fileName;
            videoId = FilenameUtils.removeExtension(fileName);
            if(videoId.indexOf('_') >= 0){
                videoId = videoId.substring(videoId.indexOf("_") + 1);
            }
            File thumbFile = new File(outputFilePath);
            if(thumbFile.exists()){
                alreadyCreated = true;
                return  outputFilePath;
            }
            thumbnailFilePath = outputFilePath;
            logger.info("Saving thumbnail to {}", outputFilePath);
            if(THUMBNAIL_FORMAT.equals("jpg")){
                ImageIO.write(image, "JPEG", new File(outputFilePath));
            } else {
                ImageIO.write(image, THUMBNAIL_FORMAT, new File(outputFilePath));
            }
            return outputFilePath;
        }
        catch (IOException e) {
            logger.error("Exception in creating thumbnail", e);
            if(videoId != null )
                StatusManager.setStatus(videoId, Status.INTERNAL_SERVER_ERROR);
            return null;
        }
    }

    public String getThumbnailFilePath() {
        return thumbnailFilePath;
    }

    public boolean isCapture() {
        return capture;
    }
}