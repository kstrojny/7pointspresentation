package com.sevenPoints.red5Server.converter;

import com.xuggle.mediatool.*;
import org.apache.commons.io.FilenameUtils;
import org.red5.logging.Red5LoggerFactory;
import org.slf4j.Logger;

import java.awt.image.BufferedImage;

/**
 * Created by vreal on 29.09.14.
 */
public class VideoConverter {

    private static Logger logger = Red5LoggerFactory.getLogger(VideoConverter.class, "7points-red5server");

    private ImageSnapListener imageSnapListener;
    private IMediaReader mediaReader = null;
    private IMediaWriter mediaWriter = null;
    private String filePath;

    public VideoConverter(String filePath){
        this.imageSnapListener = new ImageSnapListener(filePath);
        this.filePath = filePath;
    }

    public String convert(String format){
        logger.info("Converting file {} to format {}", filePath, format);
        long startTime = System.nanoTime();
        String outputFilename = FilenameUtils.removeExtension(filePath) + "." + format;
        mediaReader = ToolFactory.makeReader(filePath);

        //methods to create thumbnail
        mediaReader.setBufferedImageTypeToGenerate(BufferedImage.TYPE_3BYTE_BGR);
        mediaReader.addListener(imageSnapListener);
        /*
        there are problems with conversion to flv format,
        flv does not support that sample rate, choose from (44100, 22050, 11025).
        This Listener change sample rate to 44100
         */

        // create a media writer
        mediaWriter = ToolFactory.makeWriter(outputFilename, mediaReader);
//        mediaWriter.setMaskLateStreamExceptions(true);
        FlashMediaAdapter flash = null;
        if("flv".equals(format)){
            logger.info("Adding flash listener, format is " + format);
            flash = new FlashMediaAdapter(mediaReader, mediaWriter);
            mediaReader.addListener(flash);
        }

        // add a writer to the reader, to create the output file
        if(flash == null){
            mediaReader.addListener(mediaWriter);
        } else {
            flash.addListener(mediaWriter);
        }
        // read and decode packets from the source file and
        // and dispatch decoded audio and video to the writer
        while (mediaReader.readPacket() == null)
            ;
        long endTime = System.nanoTime();
        logger.info("{}, {} converted in {} milliseconds", format, filePath, (endTime - startTime) / 1000000);
        return outputFilename;
    }

    public boolean isCapture() {
        return imageSnapListener.isCapture();
    }

    public String getThumbnailFilePath() {
        return imageSnapListener.getThumbnailFilePath();
    }
}
