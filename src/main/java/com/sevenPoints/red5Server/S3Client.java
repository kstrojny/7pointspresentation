package com.sevenPoints.red5Server;


import com.amazonaws.HttpMethod;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.*;
import com.amazonaws.util.StringUtils;
import com.sevenPoints.red5Server.converter.ImageSnapListener;
import com.sevenPoints.red5Server.converter.VideoConverter;
import org.apache.commons.io.FilenameUtils;
import org.red5.logging.Red5LoggerFactory;
import org.slf4j.Logger;

import java.io.*;
import java.net.URL;
import java.nio.file.FileSystems;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

/**
 * Created by vreal on 25.09.14.
 */
public class S3Client implements Callable<Map<String,String>> {

    private static Logger logger = Red5LoggerFactory.getLogger(S3Client.class, "7points-red5server");

    private static String bucketName = Application.getProp().getProperty("bucketName");
    private static String PREFIX = Application.getProp().getProperty("PREFIX");
    private AmazonS3 s3;
    private String filename;
    private String videoFormat;

    public S3Client(String filename, String videoFormat){
        this.videoFormat = videoFormat;
        this.filename = filename;
        s3 = new AmazonS3Client();
    }

    private URL getURL(String objectKey){
        java.util.Date expiration = new java.util.Date();
        long msec = expiration.getTime();
        msec += 1000 * 60 * 60 * 24 * 365 * 100; // 100 years
        expiration.setTime(msec);

        GeneratePresignedUrlRequest generatePresignedUrlRequest =
                new GeneratePresignedUrlRequest(bucketName, objectKey);
        generatePresignedUrlRequest.setMethod(HttpMethod.GET);
        generatePresignedUrlRequest.setExpiration(expiration);

        return s3.generatePresignedUrl(generatePresignedUrlRequest);
    }

    public String sendFileAndGetUrl(String sendFile, String sendFormat){
        long startTime = System.nanoTime();
        String objectKey = PREFIX + FileSystems.getDefault().getPath(sendFile).getFileName().toString().replace("_","/");
        logger.info("Uploading a new object {} to S3 from a file", sendFile);

        s3.putObject(new PutObjectRequest(bucketName, objectKey, new File(sendFile)));
        URL objectUrl = getURL(objectKey);
        logger.info("Url to file {} is {}", objectUrl.getPath(), objectUrl.toString());
        long endTime = System.nanoTime();
        logger.info("File {} send in {} milliseconds" , sendFile, (endTime - startTime) / 1000000);
        return objectUrl.toString();
    }

    public void deleteFile(String deleteName){
        File file = new File(deleteName);
        if(file.delete()){
            logger.info("{} is deleted!", deleteName);
        } else {
            logger.error("Delete operation failed with file {}", deleteName);
        }
    }
    /*
     * Call will convert @filename to @videoFormat, send it and it's thumbnail to Amazon server,
     * clean up and return map of names ands urls to saved files
     */
    @Override
    public Map<String, String> call() {
        Map<String, String> result = new HashMap<>();
        try {
            String convertedFile = filename;
            VideoConverter videoConverter = null;
            if(! videoFormat.equals(FilenameUtils.getExtension(filename))){  //need to convert
                videoConverter = new VideoConverter(filename);
                convertedFile = videoConverter.convert(videoFormat);
            }

            String videoUrl = sendFileAndGetUrl(convertedFile, videoFormat);
            result.put(videoFormat, videoUrl);
            deleteFile(convertedFile);
            if(videoConverter != null &&
                    videoConverter.isCapture() &&
                    !StringUtils.isNullOrEmpty(videoConverter.getThumbnailFilePath())){
                String thumbnailUrl = sendFileAndGetUrl(videoConverter.getThumbnailFilePath(), ImageSnapListener.THUMBNAIL_FORMAT);
                result.put(ImageSnapListener.THUMBNAIL_FORMAT, thumbnailUrl);
                deleteFile(videoConverter.getThumbnailFilePath());
            } else if(videoConverter != null){
                logger.warn("There was no thumbnail to send");
            } else {
                logger.debug("Video format is the same as filename format, there was no need to convert");
            }
        } catch (Exception e){
            logger.error("Error in processing file! ", e);
        }
        return result;
    }
}
