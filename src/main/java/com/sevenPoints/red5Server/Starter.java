package com.sevenPoints.red5Server;

import com.amazonaws.util.StringUtils;
import com.sevenPoints.red5Server.converter.ImageSnapListener;
import com.sevenPoints.red5Server.converter.VideoConverter;
import com.sevenPoints.red5Server.status.Status;
import com.sevenPoints.red5Server.status.StatusManager;
import org.apache.commons.io.FilenameUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by vreal on 01.10.14.
 * Starts two thread to convert and upload two video files, and then update database
 */
public class Starter implements Runnable {

    private String path;

    public Starter(String path){
        this.path = path;
    }

    @Override
    public void run() {
        String uploadedFilePath = null;
        Map<String, String> urls = new HashMap<>();
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Future<Map<String, String>> futureUrls = executor.submit(new S3Client(path, "mp4"));

        String secondFormat = "flv";
        if(! secondFormat.equals(FilenameUtils.getExtension(path))){
            VideoConverter videoConverter = new VideoConverter(path);
            uploadedFilePath = path;
            path = videoConverter.convert(secondFormat);
            String thumbnailFilePath = videoConverter.getThumbnailFilePath();
            if(! StringUtils.isNullOrEmpty(thumbnailFilePath)){
                urls.put(ImageSnapListener.THUMBNAIL_FORMAT, thumbnailFilePath);
            }
        }

        S3Client client = new S3Client(path, secondFormat);   //there is no need to create 3 Threads to execute 2 tasks
        String secondUrl = client.sendFileAndGetUrl(path, secondFormat);
        String fileName = FilenameUtils.getBaseName(path);
        String videoId = FilenameUtils.removeExtension(fileName);
        if(videoId.indexOf('_') >= 0){
            videoId = videoId.substring(videoId.indexOf("_") + 1);
        }
        try {
            urls.putAll(futureUrls.get());
            urls.put(secondFormat, secondUrl);
            MongoProxy.saveUrlsInDatabase(path, urls);
        } catch (InterruptedException e) {
            e.printStackTrace();
            if(videoId != null )
                StatusManager.setStatus(videoId, Status.INTERNAL_SERVER_ERROR);
        } catch (ExecutionException e) {
            if(videoId != null )
                StatusManager.setStatus(videoId, Status.INTERNAL_SERVER_ERROR);
            e.printStackTrace();
        }
        if(uploadedFilePath != null){
            client.deleteFile(uploadedFilePath);
        }
        client.deleteFile(path);

    }

    public static void main(String[] args){
        Map<String, String> map = new HashMap<>();
        map.put("a", "b");
        map.put("a", "c");
        for(String key : map.keySet()){
            System.out.println(map.get(key));
        }
    }
}
