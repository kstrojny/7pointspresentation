7points-red5server
==================

Repository for 7points Red5 server (Java)


How to set up red5 server:

We use red5 version 0.9_RC2 and java 1.7

export RED5_HOME=/usr/share/red5
export AWS_ACCESS_KEY_ID=(You should know this)
export AWS_SECRET_ACCESS_KEY=(You should know this)
export MONGO_REMOTE_UR=(You should know this)

wget http://www.red5.org/downloads/red5/0_9/red5-0.9.RC2.tar.gz
extract it and save under /usr/share/red5
cd /usr/share/red5/webapps
git clone [this repo]
cd 7points-red5server
mkdir thumbnails
mkdir streams

mvn install:install-file -DgroupId=org.red5 -DartifactId=red5-server -Dversion=0.9 -Dpackaging=jar -Dfile=/path/to/red5.jar
mvn install:install-file -DgroupId=org.slf4j -DartifactId=slf4j-api -Dversion=1.7.7 -Dpackaging=jar -Dfile=/usr/share/red5/lib/slf4j-api-1.5.8.jar

mvn install

rm /usr/share/red5/webapps/7point-red5server/WEB-INF/red5.jar
rm /usr/share/red5/webapps/7point-red5server/WEB-INF/slf4j-api-1.5.8.jar

cd /usr/share/red5
./red5.sh &